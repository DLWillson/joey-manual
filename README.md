# Joey  Manual

A manual for a magnificent dog

![a magnificent dog](Joey.png)

Joey is 95% people-loving sweetheart, 5% nervous, defensive, growly biter. This manual is to help his care-givers have fun with him, not get bitten, and not have their friends get bitten.

General Rules and/or Guidelines:

Joey likes small spaces, but he's protective of them, so he's not allowed under tables where even sensible people might follow or reach under at him. He can go into his crate, which is covered with a blanket, when he wants to be in a small space. Nobody should enter his crate, ever.

Food: Joey gets a flat scoop at about 5:30am and 5PM. The times are not important.

Crate: Joey takes rests at will and as-needed to keep him out of trouble, in his crate.

eCollar: Joey is using an eCollar to learn to follow commands, especially in stressful or distracting situations. It should be charged at night, and he should be wearing it any time his caregivers are awake and nearby. His caregivers should have the radio on or near them. The eCollar is to help him follow commands and to defuse naughty or dangerous behavior. Joey responds well to his eCollar at very low numbers. Generally, he will respond to voice alone and the eCollar can be on 0. When he does not repond promptly, increment 3-5 at a time. He rarely goes higher than 8, but depending on distractions, he may have to go as high as 20. If he's not responding at 20, make sure the collar is turned on.

Joey's leash should be on him anytime predictability is important. Rabbits, kids, and strangers impact predictability, so use the leash when any of them will be around. Joey responds well to gentle leash pressure. If he doesn't respond to gentle leash pressure, do some simple exercises to help him focus up.

Indicators of stress:

When Joey is stressed, send him to his crate for a break, or give him a few simple exercises to do.

- yawning
- lip-licking
- "whale eyes"
- growling
- hackles
- turning away

Non-indicators:

These are things that I thought were indicators of stress, but they're not. They seem to be indicators of focused effort.

- ears back
- head lowered

Commands and what they mean:

Release word: **"Break"** Take a break. You are released from whatever you were doing: sitting, laying down, heeling, or whatever.

- **"Come"** - Come quickly to within petting range, and check in with me.
- **"Sit"** - Quickly sit down where you are. Adjusting for comfort is OK. Stay sitting there until released.
- **"Down"** - Lay down comfortably where you are. Adjusting for comfort is OK. Stay laying down until released.
- **"Place"** - Get on the chair/table/platform/bed I am pointing at and sit. Stay there until released.
- **"OFF!"** - Stop that, right now! (whatever naughty thing you're doing) Don't praise compliance with OFF!, but if you want to give him a positive exercise, you can praise that, instead. If using the radio, stim at 25.
- **"Crate"** - Get completely in your crate. Stay in your crate until released.
- **"Bed"** - Get on your bed. Stay on your bed until released.
- **"Heel"** - Walk close to me, on my left, with your collar and shoulder even with my legs.

Praise words: Good, OK, thank you, yay, etc... I avoid "great" because it sounds like "crate" and "break". Joey should not release when praised.

Muzzle: Whenever Joey is going to be near someone that he has bitten or growled at, or otherwise shown unfriendliness toward, or even just lots of sue-happy strangers, muzzle him. He's generally cool with it. Even though it's not fun to be muzzled, it's much better than missing out on the fun.

Toys: Frog, orange bone, Kong, lammy

Contacts:
- David L. Willson, 720-333-5267
- Heather L. Willson, 303-229-3633
- Stevi at Off-Leash K9 (his therapist), 303-222-8036x701
